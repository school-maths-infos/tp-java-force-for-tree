Céline, le 08/11/2015 :
    Apparemment le Autowire ne fonctionne pas en environnement de test.
    Raison : Dans le controller, le println du nodeRepository donne null, puis on obtient un JavaNullPointerException
    Idée : Je pense qu'on devrait laisser tomber le fait de faire des tests automatisés et les faire nous même à la main dans notre requêteur préféré
    A la place j'ai mis dans initMongo un petit script qui met la base de données de tout le monde au même point.
    Pour l'exécuter :
        mongo init.js