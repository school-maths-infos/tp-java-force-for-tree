package com.univ.smb.forceFourTree.repositories;

import com.univ.smb.forceFourTree.models.Node;

import java.util.List;
import java.util.Map;

public interface NodeRepositoryCustom {

    List<Node> filterByFacets(Map<String, String[]> facets);
}
