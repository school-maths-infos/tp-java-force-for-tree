package com.univ.smb.forceFourTree.repositories;


import com.univ.smb.forceFourTree.models.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
import java.util.Map;

public class NodeRepositoryImpl implements NodeRepositoryCustom {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<Node> filterByFacets(Map<String, String[]> facets) {
        Query query = new Query();
        Criteria[] criteria = new Criteria[facets.size()];
        int index = 0;
        for(String category : facets.keySet()) {
            String[] keywords = facets.get(category);
            if(keywords == null) {
                criteria[index] = Criteria.where("facets." + category).exists(true);
            }
            else {
                criteria[index] = Criteria.where("facets." + category).all(facets.get(category));
            }
            index++;
        }
        Criteria criterion = new Criteria();
        query.addCriteria(criterion.andOperator(criteria));
        return mongoTemplate.find(query, Node.class);
    }


}
