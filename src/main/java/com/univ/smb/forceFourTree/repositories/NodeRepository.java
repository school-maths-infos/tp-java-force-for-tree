package com.univ.smb.forceFourTree.repositories;

import com.univ.smb.forceFourTree.models.Node;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NodeRepository extends MongoRepository<Node, String>, NodeRepositoryCustom {
    Node findByName(String name);

    List<Node> findByType(String category);
}
