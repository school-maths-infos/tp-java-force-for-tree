package com.univ.smb.forceFourTree.models;

import java.util.ArrayList;
import java.util.List;

public class Outlink {

    private String name;
    private List<Node> leaves;

    public Outlink() {
        this.leaves = new ArrayList<Node>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getLeaves() {
        return leaves;
    }

    public void setLeaves(List<Node> leafs) {
        this.leaves = leafs;
    }
}
