package com.univ.smb.forceFourTree.models;

import java.util.ArrayList;
import java.util.List;

public class Tree {

    private String name;
    private List outlinks;
    private boolean error;

    public Tree() {
        this.outlinks = new ArrayList();
    }

    public void addOutlink(Outlink outlink) {
        this.outlinks.add(outlink);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List getOutlinks() {
        return outlinks;
    }

    public void setOutlinks(List outlinks) {
        this.outlinks = outlinks;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
