package com.univ.smb.forceFourTree.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Map;

@Document(collection="forcefourtree")
@XmlRootElement(name = "node")
public class Node {

    @Id
    String id;

    String name;

    String type;

    Map<String, List<String>> facets;

    List<String> keywords;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, List<String>> getFacets() {
        return facets;
    }

    public void setFacets(Map<String, List<String>> facets) {
        this.facets = facets;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keyWords) {
        this.keywords = keyWords;
    }
}
