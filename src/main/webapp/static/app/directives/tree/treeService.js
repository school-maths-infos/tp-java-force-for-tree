(function () {
    "use strict";

    var module = angular.module("services");

    module.factory("treeService", ["$q", "Restangular", function ($q, Restangular) {
        var TreeService = {};

        TreeService.get = function (params) {
            var data = $q.defer();

            Restangular
                .one('tree')
                .one('filter.json' + (params !== '' ? '?' + params : ''))
                .get()
                .then(function(response) {
                    if(!response.error) {
                        data.resolve(response);
                    }
                });

            return data.promise;
        };

        return TreeService;
    }]);

})();