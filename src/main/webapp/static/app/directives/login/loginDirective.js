(function () {
    "use strict";

    var application = angular.module('login');

    application.directive('login', function(){
        return {
            restrict: 'E',
            replace: true,
            transclude: false,
            scope: false,
            templateUrl: '/static/app/directives/login/login.html',
            controller: 'loginController'
        };
    });
})();