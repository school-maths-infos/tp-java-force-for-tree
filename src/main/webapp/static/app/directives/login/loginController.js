(function () {
	"use strict";

	var module = angular.module('login');

	/**
	 * Controller for the name form
	 */
	module.controller('loginController', ["$rootScope", "$scope", "Restangular", function($rootScope, $scope, Restangular) {
		$scope.user = {
			name:"",
			password:"",
			logged:false
		};


		$scope.initUser = function() {
			$scope.user.name = "";
			$scope.user.password = "";
			$scope.user.logged = false;

		};

		$scope.login = function(){

			$scope.user.logged = true;

            Restangular
				.one('login')
                .post('', '', $scope.user,{"Content-Type":"application/x-www-form-urlencoded"})
                .then(function(response) {
					sessionStorage.setItem("roles", JSON.stringify(response.roles));
					updateRole();
                });
		};

		$scope.logout = function(){
			$scope.initUser();

			Restangular
				.one('logout')
				.get()
				.then(function(response) {
					sessionStorage.clear();
					updateRole();
					window.location.href = "/";
				});
		};

		function updateRole() {
			$rootScope.hasAuthoritiesToManageUser = containsRolesToManageUser();
			$rootScope.hasAuthoritiesToAdd = containsRolesToAdd();
			$rootScope.hasAuthoritiesToUpdateOrRemove = containsRolesToUpdateAndRemove();

			function containsRolesToManageUser() {
				if (sessionStorage.hasOwnProperty("roles")) {
					var roles = JSON.parse(sessionStorage.getItem("roles"));
					return roles.indexOf("ROLE_ADMIN") > 0;
				} else {
					return false;
				}
			}

			function containsRolesToUpdateAndRemove() {
				if(sessionStorage.hasOwnProperty("roles")) {
					var roles = JSON.parse(sessionStorage.getItem("roles"));
					return roles.indexOf("ROLE_ADMIN") > 0
						|| roles.indexOf("ROLE_MODERATOR") > 0;
				} else {
					return false;
				}
			}

			function containsRolesToAdd() {
				if (sessionStorage.hasOwnProperty("roles")) {
					var roles = JSON.parse(sessionStorage.getItem("roles"));
					return roles.indexOf("ROLE_ADMIN") > 0
						|| roles.indexOf("ROLE_MODERATOR") > 0
						|| roles.indexOf("ROLE_COMMITTER") > 0;
				} else {
					return false;
				}
			}
		}
	}]);
})();