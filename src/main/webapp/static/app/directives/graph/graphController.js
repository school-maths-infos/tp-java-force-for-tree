/**
 * Fichier non utilise car non termine de developper => NE MARCHE PAS
 */
(function () {
	"use strict";

	var module = angular.module('graph');

	/**
	 * Controller for the login form
	 */
	module.controller('graphController', ["$scope", function($scope,$http){
		

		$scope.choix = "Genre";

		$scope.majChoix = function (choix){
			var url1 = $scope.choix.split("=");
			var url2 = $scope.choix.split("&");
			if (url1.length == url2.length) {
				$scope.choix = $scope.choix + "=" + choix;
		 	}
			else {
				$scope.choix = $scope.choix + "&" + choix;
         	}
			$scope.majGraph();
		};

		$scope.majGraph = function(){
			$scope.grapheDatas = {
				"nodes":[
				],
				"links":[
				]
			};
			var res = $http.get("localhost:8181/tree/filter.json?"+ $scope.choix);
			$scope.grapheDatas.nodes.add({"name":res.name,"group":1,"mySize":50});
			for (var mNoeud  in res.outlinks){
				$scope.grapheDatas.nodes.add({"name":mNoeud.name,"group":2,"mySize":30});
				var position = ($scope.grapheDatas.nodes.size -1);
				$scope.grapheDatas.links.add({"source":0,"target":position,"value":4});
				for (var sNoeud in mNoeud.leaves){
					$scope.grapheDatas.nodes.add({"name":sNoeud.name,"group":3,"mySize":10});
					var position2 = ($scope.grapheDatas.nodes.size -1);
					$scope.grapheDatas.links.add({"source":position,"target":position2,"value":4});
				}
			}

		};

		 $scope.grapheDatas = {
		 	"nodes":[
		 	],
		 	"links":[
		 	]
		 };

		$scope.majGraph();

	}]);
})();