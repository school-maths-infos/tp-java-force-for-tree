(function () {
    "use strict";

    var module = angular.module('controllers');

    module.controller('userController', ["$rootScope", "$scope", "$location", "userService", function($rootScope, $scope, $location, userService) {
        $scope.keywordItems = [];
        $scope.isUserPage = $location.search().create ||  $location.search().update ? true : false;
        $scope.isUserCreation = $location.search().create ? true : false;
        $scope.totalPages = 0;
        $scope.currentPage = 0;

        if($scope.isUserCreation) {
            $rootScope.newUser = {};
        }

        $scope.updateCurrentUser = function(user) {
            $rootScope.newUser = user;
        };

        $scope.deleteCurrentUser = function(user) {
            userService.delete(user)
                .then(function() {
                    $scope.users.forEach(function(userInt,index) {
                        if (userInt.id === user.id) {
                            $scope.users.splice(index,1);
                        }
                    });
                });
        };

        $scope.getAllUsers = function (page) {
            userService.getAllUsers(page)
                .then(function(data) {
                    $scope.users = data.content;
                    $scope.totalPages = new Array(data.totalPages);
                    $scope.currentPage = data.number;
                });
        };

        $scope.addUser = function() {
            if(!$scope.isUserCreation) {
                userService.update($scope.newUser)
                    .then(function (data) {
                        $rootScope.newUser = {};
                    });
            } else {
                userService.add($scope.newUser)
                    .then(function (data) {
                        $scope.newUser = {};
                    });
            }
        };
    }]);
})();